<#
    .SYNOPSIS
     This script clears filesystem space on Citrix servers

    .DESCRIPTION
     This script clears the font-cache files on Citrix servers and, optionally,
     can clear the Recycling Bin as well.

    .PARAMETER ClearRecyclingBin
     Use of this switch will cause the script to permanently delete all items from the recycling bin.

    .EXAMPLE
     PS> .\Clear-CitrixDiskSpace.ps1

     This example will delete the font-cache files, but not the files in the recycling bin, on the
     computer where the script is run.

    .EXAMPLE
     PS> .\Clear-CitrixDiskSpace.ps1 -ClearRecyclingBin

     This example will delete the font-cache files _and_ the files in the recycling bin on the
     computer where the script is run.

    .LINK
     [The Project](https://gitlab.maritz.com/AutoMon/Clear-CitrixServerDiskSpace)
     [The Change Log](https://gitlab.maritz.com/AutoMon/Clear-CitrixServerDiskSpace/blob/master/CHANGELOG.md)
     [Ask for Help in Chat](https://chat.maritz.com/it-services/channels/powershell)

#>
[cmdletbinding()]
Param (
    [switch]$ClearRecyclingBin
)

$FilesToRemove = Get-ChildItem -Path "C:\Windows\ServiceProfiles\LocalService\AppData\Local\FontCache-S*"
If ($FilesToRemove.Count -ge 1) {
    $FilesToRemoveSize = $FilesToRemove.Length | Measure-Object -Sum | ForEach-Object {$_.Sum / 1MB}
    Write-Verbose "Clearing font-cache files; this will save $FilesToRemoveSize MB of space"
    Remove-Item -Path $FilesToRemove.FullName
}

If ($PSBoundParameters.ContainsKey("ClearRecyclingBin") -and ($ClearRecyclingBin -eq $true)) {
    $RecyclingBinFiles = Get-ChildItem -Path "C:\`$Recycle.bin\" -Recurse -Force
    $RecyclingBinFilesSpace = $RecyclingBinFiles.Length | Measure-Object -Sum | ForEach-Object {$_.Sum / 1MB}
    Write-Verbose "Clearing recycling bin files; this will save $RecyclingBinFilesSpace MB of space"
    Remove-Item -Path $RecyclingBinFiles.FullName
}