# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.0] - 2017-01-27
This release indicates that we've gone production ready!

From this point on, any changes to the code which will break existing scripts will be marked with a [_major_](http://semver.org/#spec-item-8) version increase.

### Added
+ The script itself! It will clear font-cache files and includes an optional switch for clearing the recycling bin, too.