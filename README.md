[![build status](http://gitlab.maritz.com/AutoMon/Clear-CitrixServerDiskSpace/badges/master/build.svg)](http://gitlab.maritz.com/AutoMon/Clear-CitrixServerDiskSpace/commits/master)
[![coverage report](http://gitlab.maritz.com/AutoMon/Clear-CitrixServerDiskSpace/badges/master/coverage.svg)](http://gitlab.maritz.com/AutoMon/Clear-CitrixServerDiskSpace/commits/master)
# Clear-CitrixDiskSpace [`[1.0.0]`](CHANGELOG.md#100---2017-01-27)

This script is used to clear filesystem disk space on the Citrix servers; in particular, the font-cache files tend to grow in size and need deleting.
Optionally, this script can also be used to clear the recycling bin _in addition to_ the font-cache files.

## Example - Clearing FontCache Files _Only_
```powershell
# First, open a powershell prompt **on the Citrix Server**
# Secondly, navigate to where you have saved the script
Push-Location -Path "C:\Path\To\The\Script\Folder"
# Now we execute the script
.\Clear-CitrixDiskSpace.ps1 -Verbose
# You should get a few messages about how much space is being saved, but no other output.
```

## Example - Clearing FontCache Files _and_ the Recycling Bin
```powershell
# First, open a powershell prompt **on the Citrix Server**
# Secondly, navigate to where you have saved the script
Push-Location -Path "C:\Path\To\The\Script\Folder"
# Now we execute the script
.\Clear-CitrixDiskSpace.ps1 -ClearRecyclingBin -Verbose
# You should get a few messages about how much space is being saved, but no other output.
```

## Scheduling the Script
It's best to schedule the script to run via [Task Scheduler](https://technet.microsoft.com/en-us/library/cc748993(v=ws.11).aspx) on each server.
This way, the task can be executed on a schedule by a service account with appropriate and limited permissions.
Changes to the script's features and behavior should be worked through this project so that we can make sure only tested changes make it to production.
This way, we can also change the task simply by editing the script here.