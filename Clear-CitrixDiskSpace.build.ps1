#requires -Module InvokeBuild

<#
    .SYNOPSIS

    .DESCRIPTION
#>

Param (
)

# Build Properties
$Timestamp              = Get-date -uformat "%Y%m%d-%H%M%S"
$TestFolder             = "$PSScriptRoot/Tests"
$SpecificationTestsFile = "$TestFolder/Specifications_$Timestamp.xml"

Push-Location $PSScriptRoot

#region Testing Tasks
    # Synopsis: Warn about not empty git status if .git exists.
    task GitStatus -If (Test-Path .git) {
        $status = exec { git status -s }
        if ($status) {
            Write-Warning "Git status: $($status -join ', ')"
        }
    }

    # Synopsis: Verifies the versions in the readme and changelog match the latest update.
    task Version {
        $VersionInChangeLog = .{ switch -Regex -File .\CHANGELOG.md {'##\s+\[(\d+\.\d+\.\d+)\]' { return $Matches[1] } } }
        $VersionInReadMe = .{ switch -Regex -File .\README.md {'Clear-CitrixDiskSpace\s+\[`\[(\d+\.\d+\.\d+)\]`\]' { return $Matches[1] } } }
        assert ($VersionInChangeLog -eq $VersionInReadme) 'README and CHANGELOG versions mismatch.'
    }

    # Synopsis: Creates the folder where the test output files will be stored.
    task BuildTestFolder {
        If (-not (Test-Path $TestFolder)) {
            $Null = New-Item -Path $TestFolder -Type Directory -Force
        }
    }

    # Synopsis: Clears previous tests from the folder, if any.
    task ClearTestFolder {
        Remove-Item -Path $TestFolder\* -Force -Recurse
    }

    # Synopsis: Tests the feature specifications
    task TestSpecs {
        $SourceFiles = Get-ChildItem $PSScriptRoot/Source -Exclude "*.Tests.*" | Select-Object -ExpandProperty FullName
        $Parameters = @{
            "Path"         = "$PSScriptRoot/Specs"
            "CodeCoverage" = $SourceFiles
            "OutputFormat" = "NUnitXml"
            "OutputFile"   = $SpecificationsTestFile
            "PassThru"     = $true
        }
        $Script:SpecificationsTestResults = Invoke-Gherkin @Parameters
    }

    # Synopsis: Verify the overall code coverage of the project.
    task TestCodeCoverage TestSpecs, {
        $CommandsExecuted = $Script:SpecificationsTestResults.CodeCoverage.NumberOfCommandsExecuted
        $CommandsAnalyzed = $Script:SpecificationsTestResults.CodeCoverage.NumberOfCommandsAnalyzed
        $CodeCoverage = $CommandsExecuted / $CommandsAnalyzed
        "Code Coverage: $("{0:P2}" -f $CodeCoverage)"
        If ($env:CI_BUILD_REF_NAME,((git status)[0].Split(' ')[-1].trim()) -contains "master"){
            Assert ($CodeCoverage -gt 0.80) 'Function Code Coverage less than 80%'
        } ElseIf ($CodeCoverage -le 0.80) {
            Write-Warning "Function code coverage less than 80%! This build will FAIL IN MASTER!"
        }
    }

    # Synopsis: Tests the help content for the module.
    task TestHelp {
        Invoke-Pester $PSScriptRoot/Source/InModule.Help.Tests.ps1 -OutputFormat NUnitXml -OutputFile $HelpTestFile
    }

    # Synopsis: Verify that the project's metadocuments are included.
    task TestMetaDocuments {
        Invoke-Pester -Script $PSScriptRoot/MetaDocuments.Tests.ps1 -OutputFormat NUnitXml -OutputFile $MetaDocsTestFile
    }

    # Synopsis: Install ReportUnit for converting NUnit XML test results into a web page dashboard.
    task InstallReportUnit {
        If (-not (Test-Path "$PSScriptRoot/reportunit.exe")){
            Invoke-WebRequest -uri "https://github.com/doesitscript/PSPesterDashboardKickstarter/raw/develop/bin/ReportUnit.exe" -OutFile .\reportunit.exe
        }
    }
    # Synopsis: Compile the test results into an HTML site
    task CompileTestReport InstallReportUnit, {
         ./reportunit.exe $TestFolder $TestFolder
    }

    # Synopsis: Remove the XML test results so they're not uploaded as artifacts.
    task ClearTestResults {
        Get-ChildItem -Path $TestFolder/*.xml | Remove-Item -Force -Verbose
    }

    task TestModule Version, BuildTestFolder, ClearTestFolder, TestFunctions, TestScripts, TestHelp
    task TestDocumentation Version, BuildTestFolder, ClearTestFolder, TestHelp, TestMetaDocuments
    task TestAll TestModule,TestDocumentation, CompileTestReport, ClearTestResults, TestCodeCoverage

#endregion Testing Tasks