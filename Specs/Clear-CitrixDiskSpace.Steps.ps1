Given "the server has the font-cache files" {
    1..10 | ForEach-Object {
        $Guid = New-Guid | Select-Object -ExpandProperty Guid
        New-Item -Path "TestDrive:\FontCache-S$_" -Value $Guid
    }
    Pester\Mock -CommandName Get-ChildItem -MockWith {
        Get-ChildItem -Path "TestDrive:\FontCache-S*"
    } -ParameterFilter {$Path -eq "C:\Windows\ServiceProfiles\LocalService\AppData\Local\FontCache-S*"}
    Pester\Mock -CommandName Remove-Item -MockWith {
        Return $Path
    }
}

Given "the server does not have the font-cache files" {
    Pester\Mock -CommandName Get-ChildItem -MockWith {
        Get-ChildItem -Path "TestDrive:\FontCache-S*"
    } -ParameterFilter {$Path -eq "C:\Windows\ServiceProfiles\LocalService\AppData\Local\FontCache-S*"}
    Pester\Mock -CommandName Remove-Item -MockWith {
        Return $Path
    }
}

Given "the server has the recycling bin files" {
    1..10 | ForEach-Object {
        $Guid = New-Guid | Select-Object -ExpandProperty Guid
        New-Item -Path "TestDrive:\RecyclingBin$_" -Value $Guid
    }
    Pester\Mock -CommandName Get-ChildItem -MockWith {
        Get-ChildItem -Path "TestDrive:\RecyclingBin*"
    } -ParameterFilter {$Path -eq "C:\`$Recycle.bin\"}
    Pester\Mock -CommandName Get-ChildItem -MockWith {
        Get-ChildItem -Path "TestDrive:\FontCache-S*"
    } -ParameterFilter {$Path -eq "C:\Windows\ServiceProfiles\LocalService\AppData\Local\FontCache-S*"}
    Pester\Mock -CommandName Remove-Item -MockWith {
        Return $Path
    }
}

Given "the server does not have the recycling bin files" {
    Pester\Mock -CommandName Get-ChildItem -MockWith {
        Get-ChildItem -Path "TestDrive:\RecyclingBin*"
    } -ParameterFilter {$Path -eq "C:\`$Recycle.bin\"}
    Pester\Mock -CommandName Get-ChildItem -MockWith {
        Get-ChildItem -Path "TestDrive:\FontCache-S*"
    } -ParameterFilter {$Path -eq "C:\Windows\ServiceProfiles\LocalService\AppData\Local\FontCache-S*"}
    Pester\Mock -CommandName Remove-Item -MockWith {
        Return $Path
    }
}

When "the script executes" {
    $ScriptPath = "$(Split-Path $PSScriptRoot -Parent)\Source\Clear-CitrixDiskSpace.ps1"
    $Script:VerboseMessage = $($Script:Output = . $ScriptPath -Verbose) 4>&1
}

When "the script executes with the '-ClearRecyclingBin' switch" {
    $ScriptPath = "$(Split-Path $PSScriptRoot -Parent)\Source\Clear-CitrixDiskSpace.ps1"
    $Script:VerboseMessage = $($Script:Output = . $ScriptPath -ClearRecyclingBin -Verbose) 4>&1
}

Then "the script will delete the font-cache files" {
    $Script:Output.Count | Should -Be 10
}

Then "the script will not try to delete font-cache files" {
    $Script:Output.Count | Should -Be 0
}

Then "the script will delete the recycling bin files" {
    $Script:Output.Count | Should -Be 10
}

Then "the script will not try to delete recycling bin files" {
    $Script:Output.Count | Should -Be 0
}

Then "the verbose messages will include font-cache space saved" {
    $Message = $Script:VerboseMessage.Message | Where-Object -FilterScript {
        $_ -match "Clearing font-cache files"
    }
    $Message | Should -Not -BeNullOrEmpty
}

Then "the verbose messages will not include font-cache space saved" {
    $Message = $Script:VerboseMessage.Message | Where-Object -FilterScript {
        $_ -match "Clearing font-cache files"
    }
    $Message | Should -BeNullOrEmpty
}

Then "the verbose messages will include recycling bin space saved" {
    $Message = $Script:VerboseMessage.Message | Where-Object -FilterScript {
        $_ -match "Clearing recycling bin files"
    }
    $Message | Should -Not -BeNullOrEmpty
}

Then "the verbose messages will not include recycling bin space saved" {
    $Message = $Script:VerboseMessage.Message | Where-Object -FilterScript {
        $_ -match "Clearing recycling bin files"
    }
    $Message | Should -BeNullOrEmpty
}